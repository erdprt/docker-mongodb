mongodb-samples
=================
# Table of contents

1. [Introduction](#1-introduction)
2. [Detail](#2-Samples)
  1. [single instance](#21-single)
  2. [replica set](#22-replicatset)
  3. [sharding](#23-sharding)
  4. [cluster](#24-cluster)

# 1. Introduction
samples for mongodb

## 2.1. single

To install:
```bash
docker-compose -f docker-compose-single.yml up -d --build
```

To load:  
```bash
docker exec -it mongodb /usr/bin/mongoimport --db new_york --collection restaurants /restaurants.json
```


## 2.2. replicatset

To install:
```bash
docker-compose -f docker-compose-replicaset.yml up -d --build
```

To configure replicat set (mongo_rs):  
```bash
docker-compose -f docker-compose-replicaset.yml exec mongorsn1 sh -c "mongo --port 27017 < /scripts/init-replicas.js"
```

## 2.3. sharding

To install:
```bash
docker-compose -f docker-compose-sharding.yml up -d --build
```

To configure sharding:  
```bash
docker-compose -f docker-compose-sharding.yml exec mongo_config sh -c "mongo --port 27017 < /scripts/init-configserver.js"
sleep 10
docker-compose -f docker-compose-sharding.yml exec mongo_router sh -c "mongo --port 27017 < /scripts/init-router.js"
```

## 2.4. cluster

To install:
```bash
docker-compose -f docker-compose-cluster.yml up -d --build
```

To configure sharding:  
```bash
docker-compose -f docker-compose-cluster.yml exec mongo_config sh -c "mongo --port 27017 < /scripts/init-configserver.js"

docker-compose -f docker-compose-cluster.yml exec mongo_shard1a sh -c "mongo --port 27017 < /scripts/init-rs1.js"
docker-compose -f docker-compose-cluster.yml exec mongo_shard2a sh -c "mongo --port 27017 < /scripts/init-rs2.js"
docker-compose -f docker-compose-cluster.yml exec mongo_shard3a sh -c "mongo --port 27017 < /scripts/init-rs3.js"
sleep 10
docker-compose -f docker-compose-cluster.yml exec mongo_router sh -c "mongo --port 27017 < /scripts/init-router.js"
```



